# Réponses

## Supprimer toutes les entités docker

```sh
docker rm $(docker ps -a | cut -d' ' -f1 | grep -v CONTAINER)
docker volume rm $(docker volume ls | cut -d ' ' -f 6 | grep -v NAME)
docker image rm $(docker image ls| cut -d' ' -f14 | grep -v ID)
```

## Installer le driver loki

```sh
docker plugin install grafana/loki-docker-driver:latest --alias logi --grant-all-permissions
```

## Lancer toutes les stacks

- traefik fonctionne sur le port 81 en raison du service apache sur la machine winfoC6

```sh
docker compose -f traefik.yml -f grafana.yml -f matomo.yml -f odoo.yml -f ghost.yml -f magento.yml  up -d --build
```

## Ajouts domain local

- Associer *.td.anthonymoll.fr à 127.0.0.1

ou

```sh
echo "

### Docker domain
127.0.0.1       ghost.td.anthonymoll.fr
127.0.0.1       grafana.td.anthonymoll.fr
127.0.0.1       matomo.td.anthonymoll.fr
127.0.0.1       traefik.td.anthonymoll.fr
127.0.0.1       odoo.td.anthonymoll.fr
127.0.0.1       magento.td.anthonymoll.fr

" >> /etc/hosts
```

## Tuer tous les CONTAINER

```sh
docker compose -f traefik.yml -f grafana.yml -f matomo.yml -f odoo.yml -f ghost.yml -f magento.yml  down
```

## Liens a visiter

- http://ghost.td.anthonymoll.fr
- http://grafana.td.anthonymoll.fr
- http://matomo.td.anthonymoll.fr
- http://traefik.td.anthonymoll.fr
- http://odoo.td.anthonymoll.fr
- http://magento.td.anthonymoll.fr
