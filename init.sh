#!/bin/bash

docker rm $(docker ps -a -q) >/dev/null 1>&2

docker plugin install grafana/loki-docker-driver:latest --alias logi --grant-all-permissions

docker compose -f traefik.yml -f grafana.yml -f matomo.yml -f odoo.yml -f ghost.yml -f magento.yml  up -d --build --remove-orphans
